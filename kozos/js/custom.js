//gördítés esemény figyelése és menü sticky class váltása
window.onscroll = function () {
    let poz = window.scrollY;
    //header amire kell a class
    let mainHeader = document.querySelector('.main-header');
    //console.log(mainHeader.offsetHeight);
    if (poz > mainHeader.offsetHeight + 15) {
        mainHeader.classList.add('sticky');
    } else {
        mainHeader.classList.remove('sticky');
    }

}//window.onscroll vége
//menuToggle
let menuToggle = document.querySelector('.menu-toggle');
let mainMenu = document.querySelector('.main-header > nav > .main-menu');
console.log(menuToggle, mainMenu);
menuToggle.onclick = function () {
    console.log(mainMenu.style.display);
    if (mainMenu.style.display === '' || mainMenu.style.display === 'none') {//üres a display style (inline)
        mainMenu.style.display = 'flex';
    } else {//nem üres (flex) - állítsuk be none ra
        mainMenu.style.display = 'none';
    }

}
/**swiper script*/
const swiper = new Swiper('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    autoplay: {
        delay: 3000,
        disableOnInteraction: true,
    },
    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.myNext',
        prevEl: '.myPrev',
    },

    // And if we need scrollbar
    /* scrollbar: {
         el: '.swiper-scrollbar',
     },*/
    slidesPerView: 1,
    spaceBetween: 10,
    breakpoints: {
        640: {
            slidesPerView: 2,
            spaceBetween: 20,
        }
    }
});
